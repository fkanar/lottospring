package pl.akademiakodu.lotto;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class LottoGenerator {



    public Set<Integer> generate(){
        Random random = new Random();
        Set<Integer> set = new TreeSet<>();
            do {
                set.add(random.nextInt(49)+1);
            }while(set.size()!=6);
        return set;
    }
}
