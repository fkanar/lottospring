package pl.akademiakodu.lotto;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;

@Controller
public class HomeController {

    /*
    * @Responsebody rezultat zwrócony będzie Stringiem
    *
    * */

    @ResponseBody
    @GetMapping("/") //po tej swiezce wywolana jest metoda helol()
    public String hello(){
        return "Witaj świecie.";
    }

    @ResponseBody
    @GetMapping("/bye")
    public String bye(){
        return "Bye";
    }

    @GetMapping("/welcome")
    public String welcome(){
        return "witam"; //że ma zwrócić htmla;
        //resources/templates/witam.html
    }

    // /product
    // ma zwrocic htmla produkt.html

    @GetMapping("/product")
    public String product(){
        return "produkt";
    }

    @GetMapping("/lotto")
    public String generateLotto(ModelMap map){
        LottoGenerator lottoGenerator = new LottoGenerator();
        map.put("numbers", lottoGenerator.generate());
        return "lotto";
    }




}
